Categories:Navigation
License:Apache2
Web Site:https://github.com/microg
Source Code:https://github.com/microg/android_packages_apps_UnifiedNlp
Issue Tracker:https://github.com/microg/android_packages_apps_UnifiedNlp//issues

Name:µg UnifiedNlp (for GAPPS-free devices)
Auto Name:µg UnifiedNlp
Summary:Location provider middleware (UnifiedNlp)
Description:
Most modern ROMs come with support for non-Google geolocation providers.
On these systems UnifiedNlp can be installed as a user app to replace
the network location provider. It acts as a middleware for various plugins
aka backends, it does not provide any location lookup itself. For further
information please refer to the [https://github.com/microg/android_packages_apps_UnifiedNlp/blob/HEAD/README.md README].

List of backends for geolocation:
* [[org.microg.nlp.backend.apple]] uses Apple's Wifi database.
* [[org.microg.nlp.backend.openwlanmap]] uses OpenWlanMap.org
* [[org.gfd.gsmlocation]] uses OpenCellId (local)
* [[org.fitchfamily.android.gsmlocation]] uses GSM Celll data (local)
* [[org.fitchfamily.android.wifi_backend]] uses (on-device generated) WiFi data (local)
* [[org.microg.nlp.backend.ichnaea]] uses Mozilla Location Services
* [[org.openbmap.unifiedNlp]] uses Openbmap.org

List of backends for (reverse) geocoding:
* [[org.microg.nlp.backend.nominatim]]

After installing you have to reboot your device, install a backend service
and activate/configure it in the app settings. After this you can use
UnifiedNlp by activating network-based geolocation from Settings -> Location:
You need to select any mode but "device only".

NOTE: If you need to use UnifiedNlp beside GAPPS, you should instead install
[org.microg.nlp].

[https://github.com/microg/android_packages_apps_UnifiedNlp/releases Changelog]
.

Repo Type:git
Repo:https://github.com/microg/android_packages_apps_UnifiedNlp/

Build:1.1.3,1103
    commit=v1.1.3
    gradle=NetworkLocation

Build:1.2.0,1200
    commit=v1.2.0
    gradle=NetworkLocation
    submodules=yes

Build:1.2.2,1202
    commit=v1.2.2
    gradle=NetworkLocation
    submodules=yes

Build:1.3.0,1300
    commit=v1.3.0
    gradle=NetworkLocation
    submodules=yes

Maintainer Notes:
* UCM and AUM fail, because the app uses org.microg.nlp as package id
  in AM.xml and changes this on buildtime. This cannot be fixed in init=.
* Auto Update Mode:Version v%v
* Update Check Mode:Tags
.

Current Version:1.3.0
Current Version Code:1300
